import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

/*state存储数据*/

let state ={
  nickName:'',
  cartCount:0
};

/*mutations方法，用于修改state的值 唯一的途径*/
let mutations ={
    //更新用户信息
  updateUserInfo(state,nickName){
    state.nickName = nickName;
  },
  updateCartCount(state,cartCount){
    state.cartCount += cartCount;
  },
  initCartCount(state,cartCount){
    state.cartCount = cartCount;
  }
};

/*实例化vuex 挂载state mutations*/
const store = new Vuex.Store({
  state,
  mutations
});

export default store;
