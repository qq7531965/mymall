let express = require('express');
let router = express.Router();
let users = require('./../models/users');
require('./../util/util');
// /* GET users listing. */
// router.get('/', function (req, res, next) {
//   res.send('respond with a resource');
// });

/* 登录方法. */
router.post("/api/users/login", function (req, res, next) {
  var param = {
    userName: req.body.userName,
    userPwd: req.body.userPwd
  };
  users.findOne(param, function (err, doc) {
    if (err) {
      res.json({
        status: "1",
        msg: err.message
      });
    } else {
      if (doc) {
        res.cookie("userId", doc.userId, {
          path: '/',
          maxAge: 1000 * 60 * 60
        });
        res.cookie("userName", doc.userName, {
          path: '/',
          maxAge: 1000 * 60 * 60
        });
        //req.session.user = doc;
        res.json({
          status: '0',
          msg: '',
          result: {
            userName: doc.userName,
            userPwd: doc.userPwd
          }
        });
      } else {
        res.json({
          status: '1',
          msg: '用户名或者密码不对',
          result: ''
        });
      }
    }
  });
});

/*退出登录*/
router.post("/api/users/logOut", function (req, res, next) {
  res.cookie("userId", "", {
    path: "/",
    maxAge: -1
  });
  res.cookie("userName", "", {
    path: "/",
    maxAge: -1
  });
  res.json({
    status: "0",
    msg: '',
    result: ''
  });
});

/*是否登录*/
router.get("/api/users/checkLogin", function (req, res, next) {
  if (req.cookies.userId) {
    res.json({
      status: "0",
      msg: "",
      result: req.cookies.userName
    });
  } else {
    res.json({
      status: "1",
      msg: "未登录",
      result: ""
    });
  }
});

/*获取购物车信息*/
router.get("/api/users/cartList", (req, res, next)=> {
  let userId = req.cookies.userId;
  if (!userId){
    return;
  }
  users.findOne({'userId': userId}, function (err, doc) {
    if (err) {
      res.json({
        status: '1',
        msg: err.message,
        result: ''
      });
    } else {
      if (doc) {
        res.json({
          status: "0",
          msg: '',
          result: doc.cartList
        });
      }
    }
  });
});

/*更新购物车数量*/
router.get("/api/users/getCartCount", function(req, res, next){
  if (req.cookies && req.cookies.userId) {
    // console.log("userId:" + req.cookies.userId);
    let userId = req.cookies.userId;
    users.findOne({'userId': userId}, (err, doc) => {
      if (err) {
        res.json({
          status: '1',
          msg: err.message,
          result: ''
        });
      } else {
        let cartList = doc.cartList;
        let cartCount = 0;
        console.log(cartList);
        cartList.map(function (item) {
          cartCount += parseInt(item.productNum);
        });
        res.json({
          status: '0',
          msg: '',
          result: cartCount
        });
      }
    });
  }

});

/*购物车删除*/
router.post("/api/users/cartDel", function (req, res, next) {
  let userId = req.cookies.userId, productId = req.body.productId;
  users.update({
    userId: userId
  }, {
    $pull: {
      'cartList': {
        'productId': productId
      }
    }
  }, function (err, doc) {
    if (err) {
      res.json({
        status: '1',
        msg: err.message,
        result: ''
      });
    } else {
      res.json({
        status: '0',
        msg: '',
        result: 'suc'
      });
    }
  });
});

/*修改商品数量*/
router.post("/api/users/cartEdit", function (req, res, next) {
  let userId = req.cookies.userId,
    productId = req.body.productId,
    productNum = req.body.productNum,
    checked = req.body.checked;
  users.update({"userId": userId, "cartList.productId": productId}, {
    "cartList.$.productNum": productNum,
    "cartList.$.checked": checked
  }, function (err, doc) {
    if (err) {
      res.json({
        status: '1',
        msg: err.message,
        result: ''
      });
    } else {
      res.json({
        status: '0',
        msg: '',
        result: 'suc'
      });
    }
  });
});

/*全选*/
router.post("/api/users/editCheckAll", function (req, res, next) {
  let userId = req.cookies.userId,
    checkAll = req.body.checkAll ? '1' : '0';
  users.findOne({userId: userId}, function (err, user) {
    if (err) {
      res.json({
        status: '1',
        msg: err.message,
        result: ''
      });
    } else {
      if (user) {
        user.cartList.forEach((item) => {
          item.checked = checkAll;
        });
        user.save(function (err1, doc) {
          if (err1) {
            res.json({
              status: '1',
              msg: err1.message,
              result: ''
            });
          } else {
            res.json({
              status: '0',
              msg: '',
              result: 'suc'
            });
          }
        })
      }
    }
  });
});

/*获取用户地址*/
router.get("/api/users/getAddress", async (req, res, next) => {
  let userId = req.cookies.userId;
  await users.findOne({'userId': userId}, (err, doc) => {
    if (err) {
      res.json({
        status: '1',
        msg: err.message,
        result: ''
      });
    } else {
      res.json({
        status: '1',
        msg: '',
        result: doc.addressList
      });
    }
  });
});
// router.get("/getAddress", function (req, res, next) {
//   let userId = req.cookies.userId;
//   users.findOne({userId: userId}, function (err, doc) {
//     if (err) {
//       res.json({
//         status: '1',
//         msg: err.message,
//         result: ''
//       });
//     } else {
//       res.json({
//         status: '0',
//         msg: '',
//         result: doc.addressList
//       });
//     }
//   });
// });

//设置默认的地址
router.post("/api/users/setDefault", function (req, res, next) {
  let userId = req.cookies.userId,
    addressId = req.body.addressId;
  if (!addressId) {
    res.json({
      status: '1003',
      msg: 'addressId is null',
      result: ''
    });
  } else {
    users.findOne({'userId': userId}, function (err, doc) {
      if (err) {
        res.json({
          status: '1',
          msg: err.message,
          result: ''
        });
      } else {
        let addressList = doc.addressList;
        addressList.forEach((item) => {
          if (item.addressId === addressId) {
            item.isDefault = true;
          } else {
            item.isDefault = false;
          }
        });

        doc.save(function (err1, doc1) {
          if (err) {
            res.json({
              status: '1',
              msg: err1.message,
              result: ''
            });
          } else {
            res.json({
              status: '0',
              msg: '',
              result: ''
            });
          }
        })
      }
    })
  }
});

/*删除地址接口*/
router.post("/api/users/delAddress", function (req, res, next) {
  let userId = req.cookies.userId,
    addressId = req.body.addressId;
  users.update({
    'userId': userId
  }, {
    $pull: {
      'addressList': {
        'addressId': addressId
      }
    }
  }, function (err, doc) {
    if (err) {
      res.json({
        status: '1',
        msg: err.message,
        result: ''
      });
    } else {
      res.json({
        status: '0',
        msg: '',
        result: 'suc'
      });
    }
  });
});

/*确定订单信息*/
router.post("/api/users/payMent", function (req, res, next) {
  let userId = req.cookies.userId,
    addressId = req.body.addressId,
    orderTotal = req.body.orderTotal;
  users.findOne({'userId': userId}, function (err, doc) {
    if (err) {
      res.json({
        status: '1',
        msg: err.message,
        result: ''
      });
    } else {
      let address = '', goodsList = [];
      //获取当前用户的地址信息
      doc.addressList.forEach((item) => {
        if (addressId === item.addressId) {
          address = item;
        }
      });

      //获取用户购物车的购买商品
      doc.cartList.forEach((item) => {
        if (item.checked === '1') {
          goodsList.push(item);
        }
      });

      let platfrom = '713';
      /*随机生成0-9的数字*/
      let r1 = Math.floor(Math.random() * 10);
      let r2 = Math.floor(Math.random() * 10);
      let sysDate = new Date().Format('yyyyMMddhhmmss');
      let createDate = new Date().Format('yyyy-MM-dd hh:mm:ss');
      let orderId = platfrom + r1 + sysDate + r2;

      let order = {
        orderId: orderId,
        orderTotal: orderTotal,
        addressInfo: address,
        goodsList: goodsList,
        orderStatus: '1',
        createDate: createDate
      };
      doc.orderList.push(order);

      doc.save(function (err1, doc1) {
        if (err1) {
          res.json({
            status: '1',
            msg: err1.message,
            result: ''
          });
        } else {
          res.json({
            status: '0',
            msg: '',
            result: {
              orderId: order.orderId,
              orderTotal: order.orderTotal
            }
          });
        }
      });
    }
  });
});

/*根据订单id查询订单信息*/

router.get("/api/users/orderDetail", function (req, res, next) {
  let query = req.query;
  let userId = req.cookies.userId,
    orderId = query.orderId;
  users.findOne({'userId': userId}, function (err, userInfo) {
    if (err) {
      res.json({
        status: '1',
        msg: err.message,
        result: ''
      });
    } else {
      let orderList = userInfo.orderList;
      if (orderList.length > 0) {
        let orderTotal = 0;
        orderList.forEach((item) => {
          if (item.orderId === orderId) {
            orderTotal = item.orderTotal;
          }
        });
        if (orderTotal > 0) {
          res.json({
            status: '0',
            msg: '',
            result: {
              orderId: orderId,
              orderTotal: orderTotal
            }
          });
        } else {
          res.json({
            status: '12002',
            msg: '没有此订单！',
            result: ''
          });
        }
      } else {
        res.json({
          status: '12001',
          msg: '当前用户没有创建订单',
          result: ''
        });
      }
    }
  });
});

module.exports = router;
