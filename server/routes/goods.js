let express = require('express');
let router = express.Router();
let mongoose = require('mongoose');
let goods = require('../models/goods');
let users = require('../models/users');
//连接数据库
mongoose.connect('mongodb://127.0.0.1:27017/mymall', {useNewUrlParser: true});

mongoose.connection.on("connected", () => {
  console.log("MongoDB connected success");
});

mongoose.connection.on("disconnected", () => {
  console.log("MongoDB connected disconnected");
});

//获取商品列表信息
router.get('/api/goods/list', (req, res, next) => {
  let query = req.query;
  let page = parseInt(query.page);
  let pageSize = parseInt(query.pageSize);
  let sort = parseInt(query.priceSort);  //排序
  let skip = (page - 1) * pageSize; //指定数量
  let priceLevel = query.priceLevel;
  var priceGt ='',priceLte = '';
  let params= {};
  if (priceLevel !=='all'){
    switch (priceLevel) {
      case '0':priceGt = 0;priceLte =100;break;
      case '1':priceGt =100;priceLte =500;break;
      case '2':priceGt = 500;priceLte =1000;break;
      case '3':priceGt = 1000;priceLte =5000;break;
    }
    params = {
      productPrice:{
        $gt:priceGt,
        $lte:priceLte
      }
    }
  }
  // console.log(priceLevel);
  let goodsModel = goods.find(params).skip(skip).limit(pageSize);
  goodsModel.sort({'productPrice':sort});
  goodsModel.exec((err, doc) => {
    if (err) {
      res.json({
        status: '1',
        msg: err.message
      });
    } else {
      res.json({
        status: '0',
        msg: '',
        result: {
          count: doc.length,
          list: doc
        }
      });
    }
  });
});

//加入到购物车
router.post('/api/goods/addCart',(req,res,next) =>{
  let userId = '10001',productId = req.body.productId;
  users.findOne({'userId':userId},(err,userDoc) =>{
    if (err){
      res.json({
        status:"1",
        msg:err.message
      });
    }else{
      // console.log("userDoc:"+userDoc);
      if (userDoc){
        var goodsItem = '';
        userDoc.cartList.forEach(function (item) {
          if (item.productId === productId) {
            goodsItem = item;
            item.productNum++;
          }
        });
        if (goodsItem){
          userDoc.save(function (err2,doc2) {
            if (err2){
              res.json({
                status:"1",
                msg:err2.message
              });
            } else{
              res.json({
                status:"0",
                msg:'',
                result: doc2
              });
            }
          })
        }else{
          goods.findOne({'productId':productId},(err1,doc)=>{
            if (err1){
              res.json({
                status:"1",
                msg:err1.message
              });
            } else{
              if (doc){
                doc.productNum = 1;
                doc.checked = 1;
                userDoc.cartList.push(doc);
                userDoc.save(function (err2,doc2) {
                  if(err2){
                    // console.log("2222");
                    res.json({
                      status:"1",
                      msg:err2.message
                    })
                  }else{
                    res.json({
                      status:'0',
                      msg:'',
                      result:'suc'
                    })
                  }
                })
              }
            }
          });
        }
      }
    }
  });
});


module.exports = router;
