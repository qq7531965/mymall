let createError = require('http-errors');
let express = require('express');
let path = require('path');
let cookieParser = require('cookie-parser');
let logger = require('morgan');
let bodyParser = require('body-parser');
let ejs = require('ejs');

let indexRouter = require('./routes/index');
let usersRouter = require('./routes/users');
let goodsRouter = require('./routes/goods');

let app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.engine('.html', ejs.__express);
app.set('view engine', '.html');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(function (req, res, next) {
  if (req.cookies.userId) {
    next();
  } else {
    console.log("url:" + req.originalUrl);
    if (req.originalUrl === '/api/users/login' ||
      req.originalUrl === '/api/users/logout' || req.originalUrl.indexOf('/api/goods/list') > -1) {
      next();
    } else {
      res.json({
        status: "10001",
        msg: '当前未登录',
        result: ''
      });
    }
  }
});

app.use('/',indexRouter);
app.use(usersRouter);
app.use(goodsRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
