let mongoose = require('mongoose');
const {Schema} = mongoose;

let productSchema = new Schema({
  "productId": String,
  "productName": String,
  "productImg": String,
  "productPrice": Number,
  "productNum": Number,
  "checked": String
});

module.exports = mongoose.model('good',productSchema);  //如果good不在后面指定，默认自动加s(goods)
