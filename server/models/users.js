let mongoose = require('mongoose');
const {Schema} = mongoose;

let userSchmea = new Schema({
  "userId": String,
  "userName": String,
  "userPwd": String,
  "orderList": Array,
  "cartList": [
    {
      "productId": String,
      "productName": String,
      "productPrice": Number,
      "productImg": String,
      "checked": String,
      "productNum":Number
    }
  ],
  "addressList": [
    {
      "addressId": String,
      "userName": String,
      "streetName": String,
      "postCode": Number,
      "tel": Number,
      "isDefault":Boolean
    }
  ]
});

module.exports = mongoose.model("user",userSchmea);
